import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Knapsack {
	
	// Capacidade da mochila
	private int capacidade;	
	// Peso suportado pela mochila 
	public static int pesoSuportado;
	// Intervalo m�ximo para beneficio dos itens
	public static int beneficioRand;
	// Intervalo m�ximo para pesos dos itens
	public static int pesoRand;
	// Armazernar itens do problema
	private Item[] items;
	
	public Knapsack(Item[] items, int capacidade) {
		this.items = items;
		this.capacidade = capacidade;
	}

	public static void main(String[] args) {
		
		System.out.println("################### Problema Knapsack ###################");
		
		// Solicita o peso max aceito pela mochila, limite rand�mico para peso e benef�cio
		Scanner input = new Scanner(System.in); 
		System.out.println("\nPeso que a mochila suporta: ");
		pesoSuportado = input.nextInt();
		System.out.println("\nLimite para beneficos: ");
		beneficioRand = input.nextInt();
		System.out.println("\nLimite para pesos: ");
		pesoRand = input.nextInt();
		input.close();
		
		//Inicia a popula��o
		IniciaPopulacao();		
		
	}

	public static void IniciaPopulacao() {
		
		Random rand = new Random();		
		Item[] items = { new Item("A", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)), 
						 new Item("B", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)), 
						 new Item("C", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("D", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("E", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("F", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("G", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("H", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("I", rand.nextInt(beneficioRand), rand.nextInt(pesoRand)),
						 new Item("J", rand.nextInt(beneficioRand), rand.nextInt(pesoRand))
					   };
		
		// Peso m�ximo definido pelo usu�rio
		Knapsack knapsack = new Knapsack(items, pesoSuportado);
		// Imprime conjunto gerado com valores rand�micos
		knapsack.ExibeConjuntoBase();
		
		// Invoca o c�lculo de aptid�o
		Solucao solution = knapsack.CalculaBeneficioPeso();
		// Exibe lista dos itens inseridos na mochila
		solution.ExibeConjuntoSolucao();
		
	}
	
	public void ExibeConjuntoBase() {
		
		System.out.println("\n#################### Valores gerados ####################\n");
		
		if (items != null && items.length > 0) {
			System.out.println("Capacidade da mochila : " + capacidade);
			System.out.println("Items :\n");

			for (Item item : items) {
				System.out.println("       " + item.str());
			}
		}
	}

	public Solucao CalculaBeneficioPeso() {
		
		int tamanhoItens = items.length;		
		int[][] matrix = new int[tamanhoItens + 1][capacidade + 1];

		for (int i = 0; i <= capacidade; i++)
			matrix[0][i] = 0;
		for (int i = 1; i <= tamanhoItens; i++) {
			for (int j = 0; j <= capacidade; j++) {
				if (items[i - 1].peso > j)
					matrix[i][j] = matrix[i - 1][j];
				else
					matrix[i][j] = Math.max(matrix[i - 1][j],
							matrix[i - 1][j - items[i - 1].peso] + items[i - 1].beneficio);
			}
		}
		int res = matrix[tamanhoItens][capacidade];
		int w = capacidade;
		List<Item> itemsSolution = new ArrayList<>();

		for (int i = tamanhoItens; i > 0 && res > 0; i--) {
			if (res != matrix[i - 1][w]) {
				itemsSolution.add(items[i - 1]);
				res -= items[i - 1].beneficio;
				w -= items[i - 1].peso;
			}
		}
		return new Solucao(itemsSolution, matrix[tamanhoItens][capacidade]);
	}
}