public class Item {
	
	public String nome;
	public int beneficio;
	public int peso;
	
	public Item(String nome, int beneficio, int peso) {
		this.nome = nome;
		this.beneficio = beneficio;
		this.peso = peso;
	}
	
	public String str() {
		return nome + " => [Beneficio = " + beneficio + ", Peso = " + peso + "]";
	}

}