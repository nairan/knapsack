import java.util.List;

public class Solucao {
	
	public List<Item> items;
	public int value;
	
	public Solucao(List<Item> items, int value) {
		this.items = items;
		this.value = value;
	}
	
	// M�todo para exibir a resolu��o no console
	public void ExibeConjuntoSolucao() {
		// Vari�vel que recebe a soma dos itens colocados na mochila
		int somaPeso = 0;
		
		System.out.println("\n################### Resolu��o Knapsack ###################\n");
		
		if (items != null  &&  !items.isEmpty()){	
			
			System.out.println("Itens :\n");			
			for (Item item : items) {
				System.out.println("       " + item.str());
				somaPeso += item.peso;
			}
			System.out.println("\nBeneficio total = " + value);
			System.out.println("Peso dos itens  = " + somaPeso);
		}
	}

}